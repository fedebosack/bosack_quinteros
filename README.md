# SistComp 2019_Bosack_Quinteros
## Robot Vigilante Remoto - RVR

Trabajo final de la materia Sistemas de Computación FCEFYN - 2019

El trabajo final consistirá en contruir un robot de vigilancia de tres ruedas con dos motores RC y una cámara.
La dirección del mismo se podrá controlar a través de una aplicación web, con una interfaz de 4 botones (arriba, abajo, derecha, izquierda), y un display con 
streaming en vivo de lo que transmita el robot.

Los componentes a ser utilizados serán:

*  RaspberryPi 3 B+
*  Cámara web
*  Chasis de robot de 3 ruedas
*  Módulo driver L298n para motores DC
*  Jumpers
*  Cargador externo para RaspberryPi



URL de proyecto guía:
- https://hackaday.io/project/2483-surveillance-robot-camera-surocam